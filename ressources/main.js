var list = [
    {
        name: "Join Website",
        link: "https://join.under-wolf.eu",
        balise: "join-website"
    },
    {
        name: "API Join Website",
        link: "https://api.join.under-wolf.eu",
        balise: "join-api-website"
    },
    {
        name: "UnNatural Website",
        link: "https://unnatural.fr",
        balise: "unnatural-website"
    },
    {
        name: "Epistar Website",
        link: "https://epistar.under-wolf.eu",
        balise: "epistar-website"
    }
];

function put_online(name) {
    $("#" + name).removeClass("label-danger").addClass("label-success");
    $("#" + name).text("Operational");
    return 0;
}

function put_offline(name) {
    $("#" + name).removeClass("label-success").addClass("label-danger");
    $("#" + name).text("Not Operational");
    return 1;
}

function check() {
    var t_status = 0;
    list.forEach(element => {
        $.ajax({
            url: element.link,
            type: 'GET',
            crossDomain: true,
            fail: function (xhr, textStatus, errorThrown) {
                console.log('request failed');
            },
            complete: function (xhr) {
                if (xhr.status != 200 || xhr.responseText == '{"error":{"name":"MongoError"}}')
                    t_status += put_offline(element.balise);
                else
                    t_status += put_online(element.balise);
                if (t_status == 0)
                    $(".global-status").css("display", "none");
                else
                    $(".global-status").css("display", "inherit");
            }
        });
    });
}

function init_list() {
    list.forEach(element => {
        $('.list-group').append('\
            <div class="list-group-item">\
                <img src=https://www.google.com/s2/favicons?domain=' + element.link + '\>\
                <a target = "_blank" href = "'+ element.link + '" >\
                    <h4 class="list-group-item-heading">\
                        '+ element.name + '\
                    </h4>\
                </a >\
                <p class="list-group-item-text">\
                    <span id="'+ element.balise + '" class="label">Status</span>\
                </p>\
            </div > ')
    })
}

$(document).ready(function () {
    init_list();
    check();
    window.setInterval(check, 3000);
});
